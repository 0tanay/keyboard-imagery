import sys

from PySide6.QtWidgets import QFrame
from PySide6.QtWidgets import QHBoxLayout
from PySide6.QtWidgets import QApplication
application = QApplication(sys.argv)

from KeyboardImagery.key import Key



class Window(QFrame):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		self.widget_configure()
		self.widget_initialize()
		self.widget_layout_set()
		self.widget_test_keys_insert()

		self.show()

	def widget_configure(self):
		self.setMinimumSize(800, 600)

	def widget_initialize(self):
		self.setStyleSheet('background: #131313;')
		self.setWindowTitle('Test/Default')

	def widget_layout_set(self):
		layout = QHBoxLayout()
		layout.setSpacing(32)
		layout.setContentsMargins(64, 64, 64, 64)
		self.setLayout(layout)

	def widget_test_keys_insert(self):
		layout = self.layout()

		self.widget_key_0 = Key('x')
		layout.addWidget(self.widget_key_0)

		self.widget_key_1 = Key('ctrl')
		layout.addWidget(self.widget_key_1)

		self.widget_key_2 = Key('alt')
		layout.addWidget(self.widget_key_2)

		self.widget_key_3 = Key('shift')
		layout.addWidget(self.widget_key_3)



window = Window()
sys.exit(application.exec())
