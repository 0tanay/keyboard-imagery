# Keyboard Imagery

A Python package to draw keyboard-related icons in various formats.

> "a library for drawing a frame with text in it?"
                                    - _grecko_

Primarily used for rendering "key callouts" or "key prompts" in Qt applications, this package can also save icons in common image formats.
