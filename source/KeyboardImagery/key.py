from PySide6.QtWidgets import QWidget
from PySide6.QtGui import QPainter, QColor, QFont
from PySide6.QtCore import Qt, QPoint, QPointF, QSize, QRect, QFlag



font_default  = QFont('Rajdhani', 24)
font_default.setWeight(QFont.Weight.Medium)
color_keycap  = QColor.fromString('#A9A9A9')
color_keycap_top = QColor.fromString('#C3C3C3')
color_default = QColor.fromString('#232323')



class Key(QWidget):

	def __init__(self, title, parent=None):
		self.data_title = title
		super().__init__(parent=parent)

	def paintEvent(self, event):
		painter = QPainter(self)
		painter.setRenderHint(QPainter.Antialiasing, True)
		painter.setRenderHint(QPainter.TextAntialiasing, True)

		radius  = 4
		padding = 8
		size = self.sizeHint()
		self.setFixedSize(size)

		rect_body = QRect(0, 0, size.width(), size.height())
		rect_body_top = QRect(8, 8, size.width() - 20, size.height() - 20)
		rect_text = QRect(
			8 + padding,
			8 + padding,
			size.width() - (2 * padding) - 20,
			size.height() - (2 * padding) - 20
		)

		painter.setPen(Qt.NoPen)
		painter.setBrush(color_keycap)
		painter.drawRoundedRect(rect_body, radius, radius)

		painter.setBrush(color_keycap_top)
		painter.drawRoundedRect(rect_body_top, radius/2, radius/2)

		painter.setPen(color_default)
		self.setFont(font_default)
		painter.drawText(rect_text, self.data_title)

		painter.end()

	def sizeHint(self):
		if self.data_title in ('shift', ):
			return QSize(192, 96)
		return QSize(96, 96)
